/*
 * SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 * SPDX-FileCopyrightText: 2020 Noah Davis <noahadvs@gmail.com>
 */

#include "colorschememodel.h"

QVariant ColorSchemeModel::data(const QModelIndex& index, int role) const
{
}

int ColorSchemeModel::columnCount(const QModelIndex& parent) const
{
}

int ColorSchemeModel::rowCount(const QModelIndex& parent) const
{
}

QModelIndex ColorSchemeModel::parent(const QModelIndex& child) const
{
}

QModelIndex ColorSchemeModel::index(int row, int column, const QModelIndex& parent) const
{
}

bool ColorSchemeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
}

QVariant ColorSchemeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
}

bool ColorSchemeModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant& value, int role)
{
}

Qt::ItemFlags ColorSchemeModel::flags(const QModelIndex& index) const
{
}
