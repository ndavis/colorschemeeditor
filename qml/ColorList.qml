/* SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 * SPDX-FileCopyrightText: 2020 Noah Davis <noahadvs@gmail.com>
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import org.kde.kirigami 2.12 as Kirigami

Kirigami.ScrollablePage {
    id: root
    implicitWidth: Kirigami.Units.gridUnit * 12
    padding: 0
    
    Kirigami.Theme.colorSet: Kirigami.Theme.View
    
    titleDelegate: Kirigami.SearchField {
        Layout.fillWidth: true
    }
    
    /*actions {
        main: Kirigami.Action {
            displayComponent: Kirigami.SearchField {
                Layout.fillWidth: true
            }
        }
    }*/
    
    ListModel {
        id: listModel
        ListElement {
            color: "#1b1e20"
            colorRole: "Normal Background"
            colorSet: "Window"
        }
        ListElement {
            color: "#ffffff"
            colorRole: "Normal Text"
            colorSet: "View"
        }
        ListElement {
            color: "#3daee9"
            colorRole: "Normal Background"
            colorSet: "Selection"
        }
    }

    ListView {
        id: listView
        model: listModel
        section.property: "colorSet"
        section.delegate: Kirigami.ListSectionHeader {
            label: section
        }
        delegate: Kirigami.BasicListItem {
            label: model.colorRole
            subtitle: "short description"

            Rectangle {
                id: colorRoleOutlineRect
                implicitHeight: Kirigami.Units.iconSizes.medium
                implicitWidth: implicitHeight * 2
                color: Kirigami.Theme.backgroundColor
                border.color: Kirigami.Theme.textColor.hslLightness > 0.5 ? Kirigami.ColorUtils.tintWithAlpha(root.background.color, Kirigami.Theme.textColor, 0.1) : Kirigami.ColorUtils.tintWithAlpha(root.background.color, Kirigami.Theme.textColor, 0.3)
                border.width: 1
                radius: 3

                Rectangle {
                    id: colorRoleRect
                    color: model.color
                    radius: 2
                    anchors {
                        fill: parent
                        margins: 2
                    }
                }
            }
        }
    }
}

