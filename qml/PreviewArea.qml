/* SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 * SPDX-FileCopyrightText: 2020 Noah Davis <noahadvs@gmail.com>
 */

import QtQuick 2.15
import QtQuick.Controls 2.15 as T
import org.kde.kirigami 2.14 as Kirigami
import "previewareacomponents"

Kirigami.Page {
    id: root

    property real shadowSize: 64
    property real shadowYOffset: shadowSize / 4

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding)

    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.View
    clip: true

    padding: shadowSize/2

    contentItem: Kirigami.ShadowedRectangle {
        readonly property real padding: 1 // used to mimick the contrast outline windows have

        implicitWidth: Math.max(titleBar.implicitWidth, windowContent.implicitWidth) + padding*2
        implicitHeight: titleBar.implicitHeight + windowContent.implicitHeight + padding*2

        corners {
            topLeftRadius: titleBar.radius + padding
            topRightRadius: titleBar.radius + padding
        }

        shadow {
            size: shadowSize
            yOffset: shadowYOffset
            color: Qt.rgba(0, 0, 0, 0.75)
        }

        color: Qt.rgba(0, 0, 0, 0.25) // color for the top of the outline

        Rectangle {
            id: outlineGradient
            anchors.fill: parent
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "transparent"
                }
                GradientStop {
                    position: 1
                    color: Qt.rgba(0, 0, 0, 0.4)
                }
            }
        }

        TitleBar {
            id: titleBar
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                margins: parent.padding
                bottomMargin: 0
            }
        }

        WindowContent {
            id: windowContent
            anchors {
                top: titleBar.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
                margins: parent.padding
                topMargin: 0
            }
        }
    }
}
