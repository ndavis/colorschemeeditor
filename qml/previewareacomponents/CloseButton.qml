import QtQuick 2.15
import QtQuick.Shapes 1.15
import org.kde.kirigami 2.14 as Kirigami

/**
 * This component tries to faithfully copy the appearance of the Breeze close titlebar button
 */
TitleBarButton {
    id: control

    property real closeIconSize: Math.round(control.iconSize/1.5) + (Math.round(control.iconSize/1.5) % 2)
    padding:  (control.gridUnit - closeIconSize) / 2
    icon.color: Kirigami.Theme.negativeTextColor
    pressedBgColor: Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.textColor, control.icon.color, 0.5)
    hoverBgColor: Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, control.icon.color, 0.5)

    contentItem: Shape {
        layer.enabled: true
        layer.samples: 4
        implicitWidth: control.closeIconSize
        implicitHeight: control.closeIconSize
        ShapePath {
            id: shapePath
            fillColor: "transparent"
            strokeColor: control.strokeColor
            strokeWidth: Kirigami.Units.devicePixelRatio
            capStyle: ShapePath.SquareCap
            joinStyle: ShapePath.MiterJoin
            miterLimit: Kirigami.Units.devicePixelRatio * 4
            scale {
                width: control.closeIconSize
                height: control.closeIconSize
            }
            PathMultiline {
                paths: [
                    [ Qt.point(0.0,0.0),
                      Qt.point(1.0,1.0) ],
                    [ Qt.point(0.0,1.0),
                      Qt.point(1.0,0.0) ],
                ]
            }
        }
    }

    background.visible: true
}
