/* SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 * SPDX-FileCopyrightText: 2020 Noah Davis <noahadvs@gmail.com>
 */

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Templates 2.12 as T
import org.kde.kirigami 2.12 as Kirigami

T.Pane { // Shawty!
    id: root

    property real radius: 3 * Kirigami.Units.devicePixelRatio

    Kirigami.Theme.inherit: false
    //TODO: replace with titlebar colors
    Kirigami.Theme.colorSet: Kirigami.Theme.Header

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            contentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             contentHeight + topPadding + bottomPadding)

    bottomInset: -root.radius
    clip: true

    padding: Math.max(2, Math.floor(minimize.gridUnit / 4))
    spacing: padding

    contentItem: RowLayout {
        spacing: parent.spacing

        Kirigami.Icon {
            id: appIcon
            Layout.alignment: Qt.AlignLeft
            implicitWidth: implicitHeight
            implicitHeight: minimize.gridUnit
            smooth: true
            source: "preferences-desktop-color"
        }
        QQC2.Label {
            id: title
            // Keeps the title perfectly centered
            leftPadding: appIcon.implicitWidth*2
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: Kirigami.Theme.textColor
            text: "Interactive Preview Window"
            elide: Text.ElideRight
            
            MouseArea {
                anchors.fill: parent
                hoverEnabled: false
                // positionChanged() is only emitted when pressed as long as hoverEnabled is false
                onPositionChanged: {
                    if (Window.window && (typeof Window.window.startSystemMove === "function") && mouse.source === Qt.MouseEventNotSynthesized) {
                        Window.window.startSystemMove();
                        // NOTE: only way to ensure ungrabMouse() is called from QML
                        visible=false;
                        visible=true;
                    }
                }
            }
        }
        MinimizeButton {
            id: minimize
            Layout.alignment: Qt.AlignRight
        }
        MaximizeButton {
            id: maximize
            Layout.alignment: Qt.AlignRight
        }
        CloseButton {
            id: close
            Layout.alignment: Qt.AlignRight
        }
    }

    background: Rectangle {
        color: Kirigami.Theme.backgroundColor
        radius: root.radius
    }
}
