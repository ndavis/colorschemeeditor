/* SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 * SPDX-FileCopyrightText: 2020 Noah Davis <noahadvs@gmail.com>
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.14 as Kirigami
import "contentcomponents"

Kirigami.ApplicationItem {
    id: root
    Kirigami.Theme.inherit: false
    clip: true
    
//     implicitWidth: sideBarComponent.implicitWidth + mainPageComponent.implicitWidth
    //implicitHeight: Math.max(sideBarComponent.implicitHeight, mainPageComponent.implicitHeight)
    
    /*header: QQC2.ToolBar {
        //Kirigami.Theme.colorSet: Kirigami.Theme.Header
        //Kirigami.Theme.inherit: false
        spacing: Kirigami.Units.smallSpacing
        contentItem: RowLayout {
            Kirigami.Heading {
                text: "Breeze Dark"
            }
            Kirigami.ActionToolBar {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignRight
                actions: [
                    Kirigami.Action {
                        iconName: "document-save"
                        text: "Action 1"
                        shortcut: "Ctrl+S"
                        tooltip: root.fakeControlToolTip
                    },
                    Kirigami.Action {
                        iconName: "edit-undo"
                        text: "Action 2"
                        shortcut: "Ctrl+Z"
                        tooltip: root.fakeControlToolTip
                    },
                    Kirigami.Action {
                        iconName: "edit-redo"
                        text: "Action 3"
                        shortcut: "Ctrl+Shift+Z"
                        tooltip: root.fakeControlToolTip
                    }
                ]
            }
        }
    }*/
    
    readonly property string fakeControlToolTip: "This control does nothing."
    pageStack.defaultColumnWidth: Kirigami.Units.gridUnit * 7
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.Auto
//    pageStack.globalToolBar.toolbarActionAlignment: Qt.AlignLeft
    pageStack.initialPage: [sideBarComponent, mainPageComponent]

    Component {
        id: sideBarComponent
        SideBar {}
    }

    Component {
        id: mainPageComponent
        MainPage {}
    }
}
