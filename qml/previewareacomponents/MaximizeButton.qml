import QtQuick 2.15
import QtQuick.Shapes 1.15
import org.kde.kirigami 2.14 as Kirigami

/**
 * This component tries to faithfully copy the appearance of the Breeze maximize titlebar button
 */
TitleBarButton {
    id: control

    horizontalPadding: (control.gridUnit - control.iconSize) / 2
    topPadding: (control.gridUnit - control.iconSize/2) / 2 
    bottomPadding:  (control.gridUnit - control.iconSize/2) / 2

    contentItem: Shape {
        layer.enabled: true
        layer.samples: 4
        implicitWidth: control.iconSize
        implicitHeight: control.iconSize/2
        ShapePath {
            id: shapePath
            fillColor: "transparent"
            strokeColor: control.strokeColor
            strokeWidth: Kirigami.Units.devicePixelRatio
            capStyle: ShapePath.SquareCap
            joinStyle: ShapePath.MiterJoin
            miterLimit: Kirigami.Units.devicePixelRatio * 4
            scale {
                width: control.iconSize
                height: control.iconSize/2
            }
            PathPolyline {
                path: [
                    Qt.point(0.0,1.0),
                    Qt.point(0.5,0.0),
                    Qt.point(0.5,0.0),
                    Qt.point(1.0,1.0)
                ]
            }
        }
    }
}
