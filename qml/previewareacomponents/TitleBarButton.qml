import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import org.kde.kirigami 2.14 as Kirigami

/**
 * This component tries to faithfully copy the appearance of Breeze minimize, maximize and close buttons
 */
T.AbstractButton {
    id: control

    property color strokeColor: control.background.visible ? Kirigami.Theme.backgroundColor : control.icon.color
    property color pressedBgColor: Kirigami.ColorUtils.tintWithAlpha(Kirigami.Theme.backgroundColor, control.icon.color, 0.5)
    property color hoverBgColor: Kirigami.Theme.textColor
    property int gridUnit: Kirigami.Units.gridUnit + (Kirigami.Units.gridUnit % 2)
    property real offset: Kirigami.Units.devicePixelRatio
    property real iconSize: Math.round(gridUnit/1.5) + (Math.round(gridUnit/1.5) % 2)

    implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                            implicitContentWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                             implicitContentHeight + topPadding + bottomPadding)

//     padding: (control.gridUnit - control.iconSize) / 2

    icon.color: Kirigami.Theme.textColor

    hoverEnabled: true

    background: Rectangle {
        implicitHeight: control.gridUnit
        implicitWidth: control.gridUnit
        radius: height/2
        visible: control.pressed || control.checked || control.hovered
        color: {
            if (control.pressed) {
                return control.pressedBgColor;
            } else if (control.hovered) {
                return control.hoverBgColor;
            } else {
                return control.icon.color;
            }
        }
    }
}
