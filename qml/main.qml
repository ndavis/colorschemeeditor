/*
 * Copyright 2020 Noah Davis <noahadvs@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.14 as Kirigami
import org.kde.newstuff 1.0 as KNS

// import org.kde.kcolorschemeeditor 1.0

QQC2.ApplicationWindow {
    id: root
    minimumWidth: 1024//Math.max(contentRowLayout.implicitWidth, header.implicitWidth)
    minimumHeight: 720

    // TODO: use real colorscheme name
    title: "Breeze Dark"
    visible: true
    color: Kirigami.Theme.backgroundColor

    header: QQC2.ToolBar {
        Kirigami.Theme.colorSet: Kirigami.Theme.Header
        Kirigami.Theme.inherit: false
        spacing: Kirigami.Units.smallSpacing
        contentItem: RowLayout {
            spacing: parent.spacing
            Kirigami.SearchField {
                id: searchField
                Layout.preferredWidth: colorList.width - header.leftPadding - header.spacing
    //             onAccepted: console.log("Search text is " + searchField.text)
            }
            QQC2.ToolSeparator {
                id: toolSeparator
                leftPadding: 0
                rightPadding: 0
                Layout.fillHeight: true
            }
            Kirigami.ActionToolBar {
                id: actionToolBar
                Layout.fillWidth: true
                Layout.fillHeight: true
                actions: [
                    Kirigami.Action {
                        iconName: "color-picker"
                        text: "Pick Color Role"
                        shortcut: "Ctrl+L"
                        tooltip: "Pick a color role by clicking on a part of the preview window" + " (" + shortcut + ")"
                    },
                    Kirigami.Action {
                        iconName: "document-save"
                        text: "Save"
                        shortcut: "Ctrl+S"
                        tooltip: "Save changes to the color scheme" + " (" + shortcut + ")"
                    },
                    Kirigami.Action {
                        iconName: "document-save-as"
                        text: "Save As..."
                        shortcut: "Ctrl+Shift+S"
                        tooltip: "Save the color scheme with any changes as a new file" + " (" + shortcut + ")"
                    },
                    Kirigami.Action {
                        iconName: "edit-undo"
                        text: "Undo"
                        shortcut: "Ctrl+Z"
                        tooltip: "Undo changes to the color scheme" + " (" + shortcut + ")"
                    },
                    Kirigami.Action {
                        iconName: "edit-redo"
                        text: "Redo"
                        shortcut: "Ctrl+Shift+Z"
                        tooltip: "Redo changes to the color scheme" + " (" + shortcut + ")"
                    },
                    Kirigami.Action {
                        iconName: "edit-reset"
                        text: "Reset All"
                        shortcut: "Ctrl+R"
                        tooltip: "Reset the color scheme to the last saved state" + " (" + shortcut + ")"
                    }
                    // Upload button from old kcolorscheme UI isn't available for QML
                ]
            }
        }
    }

    RowLayout {
        id: contentRowLayout
        anchors.fill: parent
        spacing: 0
        ColorList {
            id: colorList
            Layout.fillHeight: true
            Layout.minimumWidth: colorList.implicitWidth
        }
        QQC2.ToolSeparator {
            leftPadding: 0
            rightPadding: 0
            topPadding: 0
            bottomPadding: 0
            Layout.fillHeight: true
        }
        PreviewArea {
            id: previewArea
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.minimumWidth: previewArea.implicitWidth
//             Layout.maximumWidth: previewArea.implicitWidth
//             Layout.minimumHeight: previewArea.implicitHeight
        }
    }
}
