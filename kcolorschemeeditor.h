/* SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
 * SPDX-FileCopyrightText: 2020 Noah Davis <noahadvs@gmail.com>
 */

#ifndef KCOLORSCHEMEEDITOR_H_INCLUDED
#define KCOLORSCHEMEEDITOR_H_INCLUDED

#include <KColorScheme>

#endif // KCOLORSCHEMEEDITOR_H_INCLUDED
